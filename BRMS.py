from DBMS import insert_closely_related_product, get_related_products
from ProductRecommendationEngine import generate_recommendations
from ProductResolutionEngine import resolve_products, fill_recommendations_with_sales
from ProfileRecommendationEngine import get_profile_recommendations


def generate_product_recommendations(shopping_list, required_items=4):
    """
    Generates product recommendations based on shopping list.
    :param shopping_list:
    :param required_items:
    :return list[recommendations]:
    """
    recommendationsdict = {
        "null": [],
        "categorynota": [],
        "categoryta": [],
        "subcategorynota": [],
        "subcategoryta": [],
        "subsubcategorynota": [],
        "subsubcategoryta": [],
        "allfieldsnota": [],
        "allfields": []
    }
    recommendations = get_related_products(str(shopping_list))
    if recommendations:  # Already in DB
        recommendations_list = []
        for recommendation in recommendations:
            recommendations_list.append(recommendation[0])
        return recommendations_list
    else:  # Add to db
        for product in shopping_list:
            recommendation = generate_recommendations(product)
            recommendationsdict[recommendation[0]].append(recommendation[1])
        recommendation_list = resolve_products(recommendationsdict, required_items, shopping_list)
        for item in recommendation_list:
            insert_closely_related_product(str(shopping_list), item)
        return recommendation_list


def generate_profile_recommendations(profile_id, required_items=4):
    """
    Generates recommendations based on profile id, then fills the recommendations with random sales when too few are
    generated.
    :param profile_id:
    :param required_items:
    :return list[recommendations]:
    """
    recommendations = get_profile_recommendations(profile_id, required_items)
    if recommendations:
        if type(recommendations) == dict:
            return get_profile_recommendations(recommendations['product_list'], required_items)
        elif len(recommendations) < required_items:
            fill_recommendations_with_sales(recommendations, required_items, [])
        else:
            return recommendations

    else:
        recommendations = fill_recommendations_with_sales([], required_items, [])
        return recommendations
