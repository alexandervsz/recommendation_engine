import psycopg2


def open_connection(sql, is_returning):
    """
    Executes a query, returns the values if required.
    :param sql:
    :param is_returning
    :return cursor or void:
    """
    try:
        conn = psycopg2.connect(host='localhost', database='Business_rules', user='postgres', password='postgres')
        cursor = conn.cursor()
        cursor.execute(sql)
        conn.commit()

        if is_returning == 1:
            value = cursor.fetchone()
            return value

        if is_returning == 2:
            value = cursor.fetchall()
            return value
        cursor.close()

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)


def get_product(product_id):
    """
    fetches data for specified product id, then turns it into a dict.
    :param product_id:
    :return dict{property_name: value}:
    """
    product_id = product_id.replace("\'", "\'\'")
    sql = f"SELECT * FROM products WHERE id = \'{product_id}\';"
    product = open_connection(sql, 1)
    product_dict = {
        "id": product_id,
        "name": product[1],
        "brand": product[2],
        "type": product[3],
        "category": product[4],
        "subcategory": product[5],
        "subsubcategory": product[6],
        "targetaudience": product[7],
        "MSRP": product[8],
        "discount": product[9],
        "sellingprice": product[10],
        "deal": product[11],
        "description": product[12]
    }
    return product_dict


def get_all_products():
    """
    fetches all products.
    :return cursor:
    """
    sql = f"SELECT id FROM products WHERE 1=1;"
    return open_connection(sql, 2)


def get_null_products(product_id):
    """
    Fetches all products where category is null and deal is not null, excluding specified product id.
    :param product_id:
    :return cursor:
    """
    product_id = product_id.replace("\'", "\'\'")
    sql = f"SELECT id FROM products WHERE id != \'{product_id}\' AND category IS NULL AND deal IS NOT NULL;"
    return open_connection(sql, 2)


def get_category(product_id, category, target_audience):
    """
    Fetches all products where category matches category, and target audience matches target audience,
    excluding specified product id.
    :param product_id:
    :param category:
    :param target_audience:
    :return cursor:
    """
    product_id = product_id.replace("\'", "\'\'")
    if target_audience:
        target_audience = target_audience.replace("\'", "\'\'")
    try:  # Because someone thought it to be a good idea to add a list to category, this monstrosity is needed.
        if type(eval(category)) == list:
            category_list = eval(category)
            category = category_list[0]
            subcategory = category_list[1]
            subsubcategory = category_list[2]
            return get_all_fields(product_id, category, subcategory, subsubcategory,
                                  target_audience)  # Will be labeled wrongly, but that's a sacrifice I'm willing to make.

    except:  # if eval throws error it's not a list.
        pass
    category = category.replace("\'", "\'\'")
    sql = f"SELECT id FROM products WHERE id != \'{product_id}\' AND category = \'{category}\' " \
          f"AND targetaudience = \'{target_audience}\';"
    return open_connection(sql, 2)


def get_sub_category(product_id, category, subcategory, target_audience):
    """
    Fetches all products where category matches category, subcategory matches subcategory, and target audience
    matches target audience, excluding excluding specified product id.
    :param product_id:
    :param category:
    :param subcategory:
    :param target_audience:
    :return cursor:
     """
    product_id = product_id.replace("\'", "\'\'")
    category = category.replace("\'", "\'\'")
    if target_audience:
        target_audience = target_audience.replace("\'", "\'\'")
    subcategory = subcategory.replace("\'", "\'\'")
    sql = f"SELECT id FROM products WHERE id != \'{product_id}\' AND category = \'{category}\'" \
          f" AND subcategory = \'{subcategory}\' AND targetaudience = \'{target_audience}\';"
    return open_connection(sql, 2)


def get_sub_sub_category(product_id, category, subsubcategory, target_audience):
    """
    Fetches all products where category matches category, subsubcategory doesn't match subsubcategory,
    and target audience matches target audience, excluding excluding specified product id.
    :param product_id:
    :param category:
    :param subsubcategory:
    :param target_audience:
    :return cursor:
    """
    product_id = product_id.replace("\'", "\'\'")
    category = category.replace("\'", "\'\'")
    subsubcategory = subsubcategory.replace("\'", "\'\'")
    if target_audience:
        target_audience = target_audience.replace("\'", "\'\'")
    sql = f"SELECT id FROM products WHERE id != \'{product_id}\' AND category = \'{category}\'" \
          f" AND subsubcategory != \'{subsubcategory}\' AND targetaudience = \'{target_audience}\';"
    return open_connection(sql, 2)


def get_all_fields(product_id, category, subcategory, subsubcategory, target_audience):
    """
    Fetches all products where category matches category, subcategory matches subcategory,
    subsubcategory doesn't match subsubcategory, and target audience matches target audience,
    excluding excluding specified product id.
    :param product_id:
    :param category:
    :param subcategory:
    :param subsubcategory:
    :param target_audience:
    :return cursor:
    """
    product_id = product_id.replace("\'", "\'\'")
    category = category.replace("\'", "\'\'")
    subcategory = subcategory.replace("\'", "\'\'")
    subsubcategory = subsubcategory.replace("\'", "\'\'")
    if target_audience:
        target_audience = target_audience.replace("\'", "\'\'")
    sql = f"SELECT id FROM products WHERE id != \'{product_id}\' AND category = \'{category}\'" \
          f" AND subcategory = \'{subcategory}\' AND subsubcategory != \'{subsubcategory}\' " \
          f"AND targetaudience = \'{target_audience}\';"
    return open_connection(sql, 2)


def get_sales():
    """
    Fetches all products currently on sale.
    :return cursor:
    """
    sql = f"SELECT id FROM products WHERE deal IS NOT NULL"
    return open_connection(sql, 2)


def get_sub_sub_category_by_id(product_id):
    """
    Returns the subsubcategory for specified product_id.
    :param product_id:
    :return cursor:
    """
    product_id = product_id.replace("\'", "\'\'")
    sql = f"SELECT subsubcategory FROM products WHERE id = \'{product_id}\'"
    return open_connection(sql, 1)


def get_previously_viewed(profile_id):
    """
    Returns all products which were previously recommended to profile id.
    :param profile_id:
    :return cursor:
    """
    profile_id = profile_id.replace("\'", "\'\'")
    sql = f"SELECT prodid FROM profiles_previously_viewed WHERE profid = \'{profile_id}\'"
    return open_connection(sql, 2)


def get_profiles_by_product(product_id, profile_id):
    """
    Returns all profiles which have previously viewed product.
    :param product_id:
    :param profile_id:
    :return cursor:
    """
    product_id = product_id.replace("\'", "\'\'")
    profile_id = profile_id.replace("\'", "\'\'")
    sql = f"SELECT profid FROM profiles_previously_viewed WHERE prodid = \'{product_id}\'" \
          f" AND profid != \'{profile_id}\'"
    return open_connection(sql, 2)


def get_all_profiles():
    """
    returns all profile id's
    :return cursor:
    """
    sql = f"SELECT id FROM profiles WHERE 1=1"
    return open_connection(sql, 2)


def get_related_products(product_id):
    """
    Returns all related products.
    :param product_id:
    :return cursor:
    """
    product_id = product_id.replace("\'", "\'\'")
    sql = f"SELECT related_prodid FROM closely_related_products WHERE prodid = \'{product_id}\'"
    return open_connection(sql, 2)


def get_related_profiles(profile_id):
    """
    Returns all related profiles.
    :param profile_id:
    :return cursor:
    """
    profile_id = profile_id.replace("\'", "\'\'")
    sql = f"SELECT related_profile_id FROM closely_related_profiles WHERE profile_id = \'{profile_id}\'"
    return open_connection(sql, 2)


def insert_closely_related_product(prodid, related_prodid):
    """
    Inserts a closely related product into the related products table.
    :param prodid:
    :param related_prodid:
    :return void:
    """
    prodid = prodid.replace("\'", "\'\'")
    related_prodid = related_prodid.replace("\'", "\'\'")
    sql = f"INSERT INTO closely_related_products(prodid, related_prodid)" \
          f" VALUES('{prodid}','{related_prodid}');"
    open_connection(sql, 0)


def insert_closely_related_profile(profile_id, related_profile_id):
    """
    Inserts a closely related profile into the closely related profiles table.
    :param profile_id:
    :param related_profile_id:
    :return void:
    """
    profile_id = profile_id.replace("\'", "\'\'")
    related_profile_id = related_profile_id.replace("\'", "\'\'")
    sql = f"INSERT INTO closely_related_profiles(profile_id, related_profile_id)" \
          f" VALUES('{profile_id}','{related_profile_id}');"
    open_connection(sql, 0)


def insert_closely_related_profile_score(profile_id, related_profile_id, score):
    """
    Updates the score of a closely related profile pair.
    :param profile_id:
    :param related_profile_id:
    :param score:
    :return void:
    """
    profile_id = profile_id.replace("\'", "\'\'")
    related_profile_id = related_profile_id.replace("\'", "\'\'")
    sql = f"UPDATE closely_related_profiles" \
          f"    SET score = \'{score}\' " \
          f"WHERE profile_id = \'{profile_id}\' AND related_profile_id = \'{related_profile_id}\' ;"
    open_connection(sql, 0)


def get_best_matches(profile_id):
    """
    Returns all closely related profiles from related profiles table.
    :param profile_id:
    :return cursor:
    """
    profile_id = profile_id.replace("\'", "\'\'")
    sql = f"SELECT related_profile_id FROM closely_related_profiles WHERE profile_id = \'{profile_id}\'" \
          f"ORDER BY score desc"
    return open_connection(sql, 2)
