from DBMS import get_product, get_all_fields, get_sub_sub_category, get_sub_category, get_category, get_null_products


def generate_recommendations(product_id):
    """"
    Fetches the product information for product id and then returns a list of products which match the attributes
    of product. Then returns this list with information about the quality of the recommendations,
    and the recommendations themselves.
    :param product_id:
    :return list[quality information, recommended products]:
    """
    product = get_product(product_id)
    if product['category']:
        if product['subcategory'] or product['subsubcategory']:
            if product['subsubcategory']:
                if product['subcategory']:
                    productlist = get_all_fields(product_id, product['category'], product['subcategory'],
                                                 product['subsubcategory'], product['targetaudience'])
                    if product['targetaudience']:
                        return ["allfields", productlist]
                    else:
                        return ["allfieldsnota", productlist]
                else:
                    productlist = get_sub_sub_category(product_id, product['category'],
                                                       product['subsubcategory'], product['targetaudience'])
                    if product['targetaudience']:
                        return ["subsubcategoryta", productlist]
                    else:
                        return ["subsubcategorynota", productlist]

            else:  # by deduction product has only subcategory
                productlist = get_sub_category(product_id, product['category'], product['subcategory'],
                                               product['targetaudience'])
                if product['targetaudience']:
                    return ["subcategoryta", productlist]
                else:
                    return ["subcategorynota", productlist]

        else:
            productlist = get_category(product_id, product['category'], product['targetaudience'])
            if product['targetaudience']:
                return ["categoryta", productlist]
            else:
                return ["categorynota", productlist]

    else:
        # When category is null, subcategory and subsubcategory are always null (in current dataset).
        productlist = get_null_products(product_id)
        return ["null", productlist]
