from DBMS import get_sub_sub_category_by_id, get_sales
from random import choice


def fill_recommendations_with_sales(recommendations_list, recommendations_required, shopping_list):
    """
    Fills the recommendation list with random items on sale.
    :param recommendations_list:
    :param recommendations_required:
    :param shopping_list:
    :return:
    """
    sales = get_sales()
    for x in range(0, recommendations_required):
        while True:
            random_sale = choice(sales)
            if random_sale not in shopping_list and random_sale not in recommendations_list:
                recommendations_list.append(random_sale[0])
                break
            else:
                continue
    return recommendations_list


def resolve_products(recommendations_dict, recommendations_required, shopping_list):
    """
    Loops through the dictionary and gets the x best recommendations. If there aren't enough recommendations found
    this way, random products on sale will be added until enough products are found.
    :param recommendations_dict:
    :param recommendations_required:
    :param shopping_list:
    :return list:
    """
    recommendations_list = []
    prioritised_categories = ["allfields", "allfieldsnota", "subsubcategoryta", "subsubcategorynota",
                              "subcategoryta", "subcategorynota", "categoryta", "categorynota", "null"]
    subsubcatergorylist = []
    for category in prioritised_categories:
        if category != "null":
            try:
                for product in recommendations_dict[category][0]:
                    if len(recommendations_list) < recommendations_required:
                        subsubcategory = get_sub_sub_category_by_id(product[0])
                        if subsubcategory[0]:
                            if product[0] not in shopping_list and product[0] not in recommendations_list and \
                                    subsubcategory[0] not in subsubcatergorylist:  # to get less similar products
                                recommendations_list.append(product[0])
                                if subsubcategory:
                                    subsubcatergorylist.append(subsubcategory[0])
                        else:
                            if product[0] not in shopping_list and product[0] not in recommendations_list:
                                recommendations_list.append(product[0])
                    else:
                        return recommendations_list
            except IndexError:  # no products in prioritised_category of dict, ignore.
                pass
        else:  # in case of "null" items, other null items are most similar therefore pick one at random.
            if len(recommendations_dict[category]) > 0:
                recommendations_list.append(choice(recommendations_dict[category][0])[0])
            else:
                fill_recommendations_with_sales(recommendations_list, recommendations_required - len(
                    recommendations_list), shopping_list)

    if len(recommendations_list) < recommendations_required:
        recommendations_list = fill_recommendations_with_sales(recommendations_list,
                                                               recommendations_required - len(
                                                                   recommendations_list), shopping_list)

    return recommendations_list
