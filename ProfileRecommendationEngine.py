from DBMS import get_previously_viewed, get_profiles_by_product, get_related_profiles, insert_closely_related_profile, \
    insert_closely_related_profile_score, get_best_matches


def get_profile_recommendations(profile_id, items_required):
    """
    Fetches the products previously viewed by profile, then looks at all other profiles which have bought
    the same items, then fetches all items which these profiles have bought. Then looks at each profile and scores
    them based on similarity, then choses products which these profiles have viewed, and the provided profile
    hasn't.
    :param profile_id:
    :param items_required:
    :return void or list:
    """
    previously_viewed_products = get_previously_viewed(profile_id)
    product_list = []
    if previously_viewed_products:
        for product in previously_viewed_products:
            product_list.append(product[0])

    related_profiles = get_related_profiles(profile_id)
    if related_profiles:
        make_recommendations(profile_id, product_list,  items_required)
    elif previously_viewed_products:
        for product in previously_viewed_products:
            related_profiles = get_profiles_by_product(product[0], profile_id)
            for profile in related_profiles:
                insert_closely_related_profile(profile_id, profile[0])
        related_profiles = get_related_profiles(profile_id)
        if related_profiles:
            for profile in related_profiles:
                temp_prod_list = []
                products = get_previously_viewed(profile[0])
                for product in products:
                    temp_prod_list.append(product[0])
                    score = 0
                    for temp_product in temp_prod_list:
                        if temp_product in product_list:
                            score += 1  # Products which match increase the score.
                        else:
                            score -= 1  # Products which don't match decrease the score.

                    insert_closely_related_profile_score(profile_id, profile[0], score)
        else:
            return {"product_list": product_list}  # No profiles available, make recommendations based on products.
        make_recommendations(profile_id, product_list, items_required)
    else:
        return None  # No products found, therefore no related profiles can be found.


def make_recommendations(profile_id, product_list, items_required):
    recommendations = []
    best_profiles = get_best_matches(profile_id)
    for profile in best_profiles:
        products = get_previously_viewed(profile[0])
        for product in products:
            if len(recommendations) < items_required:
                if product not in product_list and product not in recommendations:
                    recommendations.append(product)
            else:
                return recommendations
    return recommendations  # will return as many as it got, BRMS handles the rest.
