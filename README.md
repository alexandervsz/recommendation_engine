# Recommendation Engine #

## What is this repository for? ##

This project is made as an assignment for HBO-ICT at Hogeschool Utrecht.

Check results under pictures to get a preview of what kind of data is generated.

## How do I get set up? ##

This project requires psycopg2 (`pip install psycopg2`).

This project also requires the database which was provided by the school, with two added tables:
"closely_related products" and "closely_related_profiles".

To get some test recommendations run RunMe.py.

To fill the database (to save time generating recommendations during runtime), theoretically test_brms_all_products()
and test_brms_all_profiles() could be used, however they are poorly optimized and take very long to complete.

## Explanation of the business rules. ##

This project uses two types of business rules: *content filtering* and *collaborative filtering*.

### Content filtering: ###

The program checks what fields are available. Then makes a recommendation based on products which are in the same
subcategory (because subsubcategory is too specific). If this is not possible it tries to filter more globally
(e.g. by category). When there are not enough products found this way, random products on sale are chosen (since they
are sort of manually filtered, and there should always be products on sale.) Usually this leads to a lot of suggested
products, therefore each product is marked with information about how much information it contained. The program then
picks items of the best quality until it has enough items, or else it falls back to random sales.

### Collaborative filtering: ###

The program fetches all products which were previously viewed by the profile (main profile). If none are found, random
products on sale are once again chosen. Then it checks which profiles also viewed these items. After this, the program
gets all items which are previously viewed by these profiles, and adds one point for each product that matches with the
previously viewed items of the main profile. If a product doesn't match a point is deducted. This score is then added to
the database. Then the highest score is picked (most similar profile) and it's previously viewed items are fetched.
After this items are chosen which are not previously viewed by the main profile. If there aren't enough items found this
way, the program selects the next highest score. If the program runs out of related profiles, and not enough
recommendations are found, random sales are suggested. If there are no closely related profiles available, but the main
profile has previously viewed items, product based recommendations are made based on these products (see content
filtering).

## Suggestions for future updates. ##
- Improve the random sales algorithm to look at attributes(e.g target audience).
- Improve resolution engine to select only one product per subcategory.
- Impove resolution engine to look at target audiences.