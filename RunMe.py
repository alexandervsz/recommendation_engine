from BRMS import generate_product_recommendations, generate_profile_recommendations
from DBMS import get_all_products, get_product, get_all_profiles, get_previously_viewed
from random import choice
from time import time

SAMPLES = 4  # The amount of samples required


def test_brms_random():
    """
    Test/demonstration framework for BRMS system.
    :return void:
    """
    print()
    # Testing code, generates random shopping list and random profile id.
    products = get_all_products()
    shopping_list = []
    for x in range(0, 5):
        shopping_list.append(choice(products)[0])
    print("Random shopping list consists of:")
    for item in shopping_list:
        product_dict = get_product(item)
        print(product_dict['name'])
    print()

    start = time()
    recommendations_list = generate_product_recommendations(shopping_list)
    end = time()

    print("Recommendations:")
    for item in recommendations_list:
        product_dict = get_product(item)
        print(product_dict['name'])
    print(f"Recommendations generated in: {int(end - start)} seconds.")
    print()

    # profiles starts here
    profiles = get_all_profiles()
    profile = choice(profiles)[0]
    previously_viewed_products = get_previously_viewed(profile)
    print("Previously viewed items from random profile are:")
    for product in previously_viewed_products:
        print(get_product(product[0])['name'])
    print()
    if len(previously_viewed_products) > 0:
        print("This might take a while...")

    start = time()
    profile_recommendations_list = generate_profile_recommendations(profile)
    end = time()

    print()
    print("Recommendations:")
    for item in profile_recommendations_list:
        product_dict = get_product(item)
        print(product_dict['name'])
    print(f"Recommendations generated in: {int(end - start)} seconds.")


def test_brms_all_products():
    """"
    function to test whether products are generated correctly and whether there are any errors thrown.
    :return void:
    """
    products = get_all_products()
    products_list = []
    for product in products:
        products_list.append(product[0])
    products_list.reverse()
    errorlist = []
    total = len(products)
    percent = 0
    counter = 1
    start = time()
    print(f"{percent}% done!")
    for product in products_list:
        recommendations_list = generate_product_recommendations([product], SAMPLES)
        percent = counter / total
        counter += 1
        if counter % 100 == 0:
            print(f"{counter} products done!")
            eta = (((time() - start) / percent) * 100) - (time() - start)
            print(f"estimated time left: {eta / 3600} hours.")
            print(f"{len(errorlist)} errors found. ids: {errorlist}")
        if len(recommendations_list) < SAMPLES:
            errorlist.append(product)
    print(f"{len(errorlist)} errors found. ids: {errorlist}")


def test_brms_all_products_at_once():
    """"
    Creates one big shopping list of all products so that every product is tested.
    :return void:
    """
    products = get_all_products()
    products_list = []
    for product in products:
        products_list.append(product[0])
    products_list.reverse()
    recommendations_list = generate_product_recommendations(products_list, 5)
    print(recommendations_list)


def test_brms_all_profiles():
    """"
    function to test whether profiles are generated correctly and whether there are any errors thrown.
    :return void:
    """
    profiles = get_all_profiles()
    profiles_list = []
    for profile in profiles:
        profiles_list.append(profile[0])
    errorlist = []
    total = len(profiles_list)
    counter = 1
    start = time()
    print(f"{total} records.")
    for profile in profiles_list:
        profile_recommendations_list = generate_profile_recommendations(profile, SAMPLES)
        percent = counter / total
        counter += 1
        if counter % 100 == 0:
            print(f"{counter} profiles done!")
            eta = (((time() - start) / percent) * 100) - (time() - start)
            print(f"estimated time left: {eta / 31298400} years.")
            print(f"{len(errorlist)} errors found. ids: {errorlist}")
        if len(profile_recommendations_list) < SAMPLES:
            errorlist.append(profile[0])
    print(f"{len(errorlist)} errors found. ids: {errorlist}")


while True:
    try:
        tests = int(input("How many tests would you like to run: "))
        if tests > 0:
            break
    except ValueError:
        continue
for n in range(0, tests):
    test_brms_random()
